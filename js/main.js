(function($){

  // For the main nav
  $('.nav-main').hoverIntent({
    over: function(){
      $(this).find('#menu-dropdown').fadeIn('fast');
      $(this).nextAll().addClass('halfopaque');
    },
    out: function(){
      $(this).find('#menu-dropdown').fadeOut('fast');
      $(this).nextAll().removeClass('halfopaque');
    },
    timeout: 400
  });

  //For the toggle
  $('.featured-bar').hover(function(){
    $(this).children('.featured-content').show();
    }, function(){
    $(this).children('.featured-content').hide();
  });

  //For navigating between the tabs
  var tabs = $('#tabs');

  tabs.children().not(':first').hide();

  $('#pagetab-nav').on('click','li',function(e){

    var pagelink = $(this).children().attr('rel'),
        thisLi = $(this),
        linkhref = thisLi.children('a').attr('href');

    if (typeof pagelink === 'undefined' || pagelink === false) {
      tabs.children().hide();
      tabs.children(linkhref).show();
      e.preventDefault();
      $('#pagetab-nav li').removeClass('active');
      thisLi.addClass('active');
    }

  });

  // For calculating the featured column's height
  $('.featured').each(function(){
    var tdHeight = $(this).find('td').height();
    $(this).find('.featured-wrapper').height(tdHeight);
  });

  // // Sticky THEAD
  // var trHead = $('.table-header'),
  // trHeadPos = trHead.offset().top,
  // trHeadHeight = trHead.height();

  // function stickyTrHead(thClassName){
  //   if($(window).scrollTop() >= trHeadPos ){
  //     $('body').prepend('<div class="sticky-trhead"><table class="th-sticky tablesorter ' + thClassName + '"></table></div>');
  //     trHead.clone().appendTo(".th-sticky");
  //   }
  // }

  // stickyTrHead("table-homeloan-refinance-cash");

  // $(window).scroll(function(){

  //   if ($('.sticky-trhead').length === 0){
  //     if($('#tab-calculator').is(':visible')){
  //       stickyTrHead("table-homeloan-refinance-cash");
  //     }
  //   }
  //   if ($('.sticky-trhead').length === 1 && $(window).scrollTop() < trHeadPos) {
  //     $('.sticky-trhead').remove();
  //   }
  // });




}(jQuery));