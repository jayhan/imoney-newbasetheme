//include gulp
var gulp = require('gulp');

//include gulp plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');

//Lint task
gulp.task('lint',function(){
	return gulp.src('js/*.js')
	.pipe(jshint())
	.pipe(jshint.reporter('default'));
});

//SASS task
gulp.task('sass',function(){
	return gulp.src('scss/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('css'));
});

//watch file for changes
gulp.task('watch',function(){
	gulp.watch('js/*.js', ['lint']);
	gulp.watch('scss/*.scss', ['sass'])
});

//Default task
gulp.task('default', ['sass','watch']);